#include "GameManager.h"



GameManager::GameManager()
{
}

void GameManager::startGame(Player * myPlayer, Map * map)
{
	//INTRODUCTION SCREEN
	Pokedex* pokedex = new Pokedex();
	myPlayer->currentLocation = map->locationList[0];

	cout << "Hello there! What's your name?" << endl;
	getline(cin, myPlayer->name); //if we use this though how can we acoutn for erros
	cout << endl;

	cout << "Well then " << myPlayer->name << ", I want you to choose a Pokemon.\n" << endl;

	cout << "============= S T A R T E R  P O K E M O N ==================" << endl;
	cout << "        [1] Mudkip     [2] Treecko     [3] Torchic" << endl;
	cout << "           Lvl 5           Lvl 5            Lvl 5" << endl;
	cout << "         Water Type      Grass Type       Fire Type" << endl;
	cout << "=============================================================" << endl;

	while (true)
	{
		int playerPokemonChoice = 0;
		myPlayer->loopTillValidInput(playerPokemonChoice);
		cout << endl;
		if (playerPokemonChoice == 1)
		{
			cout << "Congratulations, you've received a Mudkip!" << endl;

			Pokemon* playerMudkip = new Pokemon();
			*playerMudkip = *pokedex->PokemonList[0];
			playerMudkip->levelUp(4);
			myPlayer->addPokemon(playerMudkip); //the function to push back.
			break;
		}

		else if (playerPokemonChoice == 2)
		{
			cout << "Congratulations, you've received a Treecko!" << endl;
			Pokemon* playerTreecko = new Pokemon();
			*playerTreecko = *pokedex->PokemonList[1];
			playerTreecko->levelUp(4);
			myPlayer->addPokemon(playerTreecko);
			break;
		}

		else if (playerPokemonChoice == 3)
		{
			cout << "Congratulations, you've received a Torchic!" << endl;
			Pokemon* playerTorchic = new Pokemon();
			*playerTorchic = *pokedex->PokemonList[2];
			playerTorchic->levelUp(4);
			myPlayer->addPokemon(playerTorchic);
			break;
		}

		else
		{
			cout << "Please choose a Pokemon." << endl;
		}
	}
	system("pause");
	system("cls");
	cout << "Now... it's time for you to go on your own adventure, Enjoy!" << endl;
	system("pause");


	while (true) //MAIN GAME LOOP
	{
		system("cls");
		myPlayer->displayCurrentCoordinates();

		if (myPlayer->inBounds == false)
		{
			cout << "You are out of bounds. Please go back.\n" << endl;
		}

		else
		{
			cout << "You are in " << myPlayer->currentLocation->name << "\n" << endl;
		}

		int playerChoice = 0;
		myPlayer->playerMenuDecision(playerChoice);
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');

		if (playerChoice == 1) //TO WALK
		{
			myPlayer->Walk(map);

			if (myPlayer->currentLocation->hasPokemonCenter == false || myPlayer->inBounds == false) //if there's no poke center, then its not a safe area 
			{
				int randomEncounterChance = 0;
				randomEncounterChance = rand() % 100 + 1;

				if (randomEncounterChance <= 30)  //30% chance to encounter a pokemon
				{
					myPlayer->wildPokemonEncounter(pokedex);
				}
			}
		}


		else if (playerChoice == 2) //POKEMON
		{
			myPlayer->displayPlayerPokemon();
			system("pause");
		}

		else if (playerChoice == 3) //BAG
		{
			//display inventory
			myPlayer->displayInventory();
		}

		else if (playerChoice == 4) //STATS
		{
			//display stats function in player
			myPlayer->displayStats();
			system("pause");
		}

		else if (playerChoice == 5 && myPlayer->currentLocation->hasPokemonCenter == true) //PokemonCenter
		{
			//pokecentere function
			myPlayer->healPokemon();
		}

		else if (playerChoice == 6 && myPlayer->currentLocation->hasPokemonCenter == true) //PokeMart
		{
			//pokemart function
			myPlayer->openOpenPokeMart();
		}

		else if (playerChoice == 7) //EXIT
		{
			break;
		}

		else
		{
			cout << "Please enter a valid choice." << endl;
			system("pause");
		}
	}

	cout << "Game ended. Thanks for playing!" << endl;
	system("pause");
}

