#include "Pokemon.h"
#include <math.h>
#include <time.h>
#include <string>

using namespace std;

Pokemon::Pokemon()
{
}

Pokemon::Pokemon(string name, float baseHP, int level, float baseDamage, float baseExpToNextLevel, float givenXP, pokemonType type, int levelToEvolve, Pokemon * evolvedVersion)
{
	this->name = name;
	this->baseHP = baseHP;
	this->currentHP = baseHP;

	this->level = level;
	this->baseDamage = baseDamage;
	this->currentDamage = baseDamage;

	this->exp = 0;
	this->baseExpToNextLevel = baseExpToNextLevel;
	this->givenXP = givenXP;

	this->levelToEvolve = levelToEvolve;
	this->type = type;
	this->evolvedVersion = evolvedVersion;
}

void Pokemon::levelUp(float lvlIncrease)
{
	this->exp = 0; //reset
	this->level = this->level + lvlIncrease;
	this->baseHP = floor(this->baseHP * powf(1.15F, lvlIncrease));
	this->currentHP = this->baseHP;
	this->baseDamage = floor(this->baseDamage * powf(1.1F, lvlIncrease));
	this->baseExpToNextLevel = floor(this->baseExpToNextLevel * powf(1.2F, lvlIncrease));
	this->givenXP = floor(this->givenXP * powf(1.2F, lvlIncrease));
}

void Pokemon::Evolve(/*Pokemon * pokemonToEvolve*/)
{ //for mega evolution, then probably have a temporary Pokemon* before calling the Evolve function and then use that to "Devolve" after the battle
	system("cls");
	cout << "Oh look, " << this->name << " is evolving!" << endl;
	system("pause");
	cout << "." << endl;
	system("pause");
	cout << ".." << endl;
	system("pause");
	cout << "..." << endl;

	cout << "Wow, your " << this->name << " has evolved into " << this->evolvedVersion->name << "!" << endl;
	// Pokemon* temporary = this; //holder of the stats
	//pokemonToEvolve = pokemonToEvolve->evolvedVersion; 
	//evolve, become the evolved version
	this->name = this->evolvedVersion->name;
	this->levelToEvolve = this->evolvedVersion->levelToEvolve; //also the lvl required to evolve.
	this->evolvedVersion = this->evolvedVersion->evolvedVersion;
	//transfer back stats and apply a 10% modifier to all HP and damage.
	/* pokemonToEvolve->baseHP = temporary->baseHP * 1.1F;
	pokemonToEvolve->baseDamage = temporary->baseDamage * 1.1F;
	pokemonToEvolve->level = temporary->level;
	pokemonToEvolve->baseExpToNextLevel = temporary->baseExpToNextLevel;
	pokemonToEvolve->currentHP = temporary->currentHP * 1.1F;
	pokemonToEvolve->exp = temporary->exp;
	pokemonToEvolve->givenXP = pokemonToEvolve->givenXP; */

	this->baseHP = floor(this->baseHP * 1.1F);
	this->baseDamage = floor(this->baseDamage * 1.1F);
	this->currentHP = floor(this->currentHP *1.1F);
	system("pause");
}

void Pokemon::evolutionCheck()
{
	if (this->level >= this->levelToEvolve)
	{
		this->Evolve();
	}
}

//void Pokemon::megaEvolve(Pokemon* pokemonToEvolve)
//{
//	//for mega evolution, then probably have a temporary Pokemon* before calling the Evolve function and then use that to "Devolve" after the battle
//	cout << "Oh look, " << pokemonToEvolve->name << " is evolving!" << endl;
//	system("pause");
//	cout << "." << endl;
//	system("pause");
//	cout << ".." << endl;
//	system("pause");
//	cout << "..." << endl;
//
//	Pokemon* temporary = pokemonToEvolve; //holder of the stats
//	pokemonToEvolve = pokemonToEvolve->evolvedVersion;
//	//evolve, become the evolved version
//	//transfer back stats and apply a 10% modifier to all HP and damage.
//	pokemonToEvolve->baseHP = temporary->baseHP * 1.1F;
//	pokemonToEvolve->baseDamage = temporary->baseDamage * 1.1F;
//	pokemonToEvolve->level = temporary->level;
//	pokemonToEvolve->baseExpToNextLevel = temporary->baseExpToNextLevel;
//	pokemonToEvolve->currentHP = temporary->currentHP * 1.1F;
//	pokemonToEvolve->exp = temporary->exp;
//	pokemonToEvolve->givenXP = pokemonToEvolve->givenXP;
//
//	cout << "Wow, your " << temporary->name << " has evolved into " << pokemonToEvolve->name << "!" << endl;
//}

void Pokemon::printType()
{
	string typeOfPokemon;
	switch (this->type)
	{
	case Water: typeOfPokemon = "Water"; break;
	case Fire: typeOfPokemon = "Fire"; break;
	case Grass: typeOfPokemon = "Grass"; break;
	case Electric: typeOfPokemon = "Electric"; break;
	case Ground: typeOfPokemon = "Ground"; break;
	case Rock: typeOfPokemon = "Rock"; break;
	case Dragon: typeOfPokemon = "Dragon"; break;
	case Ice: typeOfPokemon = "Ice"; break;
	case Bug: typeOfPokemon = "Bug"; break;

	case Flying: typeOfPokemon = "Flying"; break;
	case Normal: typeOfPokemon = "Normal"; break;
	case Poison: typeOfPokemon = "Poison"; break;
	case Psychic: typeOfPokemon = "Psychic"; break;
	case Ghost: typeOfPokemon = "Ghost"; break;
	case Fighting: typeOfPokemon = "Fighting"; break;
	case Dark: typeOfPokemon = "Dark"; break;
	case Steel: typeOfPokemon = "Steel"; break;
	}

	cout << typeOfPokemon;
}

void Pokemon::displayStats()
{
	Pokemon* current = this;// for less typing?

	cout << current->name << endl;
	this->printType(); //cuz enums 
	cout << " Type" << endl;												 //Type
	cout << "Level " << current->level << endl; //for less typing?			 //Level
	if (current->currentHP < 0)
	{
		cout << "HP: 0/ " << current->baseHP << endl;
	}
	else
	{
		cout << "HP: " << current->currentHP << "/" << current->baseHP << endl;  //HP
	}
	cout << "Damage: " << current->baseDamage << endl;						 //Damage
	cout << "XP: " << current->exp << "/" << current->baseExpToNextLevel << endl << endl;  //XP
}

void Pokemon::attack(Pokemon * enemyPokemon)
{
	int attackChance = 0;
	attackChance = rand() % 100 + 1;
	if (attackChance <= 80)
	{
		this->applyBonusDmg(enemyPokemon);
		enemyPokemon->currentHP -= floor(this->currentDamage);
		cout << this->name << " dealt " << floor(this->currentDamage) << " to " << enemyPokemon->name << "!" << endl;
		this->currentDamage = this->baseDamage; //reset damage after attack
	}
	else
	{
		cout << this->name << "'s attack missed!" << endl;
	}
}

void Pokemon::applyBonusDmg(Pokemon * enemyPokemon)
{
	//modify the damage value of the attacker
	if (this->type == Water)
	{
		if (enemyPokemon->type == Water || enemyPokemon->type == Grass || enemyPokemon->type == Dragon)
		{
			this->currentDamage = this->currentDamage / 2;
			cout << "It's not very effective..." << endl;
		}

		else if (enemyPokemon->type == Fire || enemyPokemon->type == Ground || enemyPokemon->type == Rock)
		{
			this->currentDamage *= 2;
			cout << "It's super effective!" << endl;
		}
	}

	else if (this->type == Fire)
	{
		if (enemyPokemon->type == Fire || enemyPokemon->type == Water || enemyPokemon->type == Rock || enemyPokemon->type == Dragon)
		{
			this->currentDamage = this->currentDamage / 2;
			cout << "It's not very effective..." << endl;
		}

		else if (enemyPokemon->type == Grass || enemyPokemon->type == Ice || enemyPokemon->type == Bug || enemyPokemon->type == Steel)
		{
			this->currentDamage *= 2;
			cout << "It's super effective!" << endl;
		}
	}

	else if (this->type == Grass)
	{
		if (enemyPokemon->type == Fire || enemyPokemon->type == Grass || enemyPokemon->type == Poison || enemyPokemon->type == Flying || enemyPokemon->type == Bug || enemyPokemon->type == Dragon || enemyPokemon->type == Steel)
		{
			this->currentDamage = this->currentDamage / 2;
			cout << "It's not very effective..." << endl;
		}

		else if (enemyPokemon->type == Water || enemyPokemon->type == Ground || enemyPokemon->type == Rock)
		{
			this->currentDamage *= 2;
			cout << "It's super effective!" << endl;
		}
	}

	else if (this->type == Electric)
	{
		if (enemyPokemon->type == Electric || enemyPokemon->type == Grass || enemyPokemon->type == Dragon)
		{
			this->currentDamage = this->currentDamage / 2;
			cout << "It's not very effective..." << endl;
		}

		else if (enemyPokemon->type == Water || enemyPokemon->type == Flying)
		{
			this->currentDamage *= 2;
			cout << "It's super effective!" << endl;
		}

		else if (enemyPokemon->type == Ground)
		{
			this->currentDamage *= 0;
			cout << "It has no effect..." << endl;
		}
	}

	else if (this->type == Ground)
	{
		if (enemyPokemon->type == Flying || enemyPokemon->type == Grass)
		{
			this->currentDamage = this->currentDamage / 2;
			cout << "It's not very effective..." << endl;
		}

		else if (enemyPokemon->type == Fire || enemyPokemon->type == Electric || enemyPokemon->type == Poison || enemyPokemon->type == Rock || enemyPokemon->type == Steel)
		{
			this->currentDamage *= 2;
			cout << "It's super effective!" << endl;
		}

		else if (enemyPokemon->type == Flying)
		{
			this->currentDamage *= 0;
			cout << "It has no effect..." << endl;
		}
	}

	else if (this->type == Rock)
	{
		if (enemyPokemon->type == Ice || enemyPokemon->type == Ground || enemyPokemon->type == Steel)
		{
			this->currentDamage = this->currentDamage / 2;
			cout << "It's not very effective..." << endl;
		}

		else if (enemyPokemon->type == Fire || enemyPokemon->type == Ice || enemyPokemon->type == Bug || enemyPokemon->type == Flying)
		{
			this->currentDamage *= 2;
			cout << "It's super effective!" << endl;
		}
	}

	else if (this->type == Dragon)
	{
		if (enemyPokemon->type == Steel)
		{
			this->currentDamage = this->currentDamage / 2;
			cout << "It's not very effective..." << endl;
		}

		else if (enemyPokemon->type == Dragon)
		{
			this->currentDamage *= 2;
			cout << "It's super effective!" << endl;
		}
	}

	else if (this->type == Ice)
	{
		if (enemyPokemon->type == Ice || enemyPokemon->type == Fire || enemyPokemon->type == Water || enemyPokemon->type == Steel)
		{
			this->currentDamage = this->currentDamage / 2;
			cout << "It's not very effective..." << endl;
		}

		else if (enemyPokemon->type == Grass || enemyPokemon->type == Ground || enemyPokemon->type == Dragon || enemyPokemon->type == Flying)
		{
			this->currentDamage *= 2;
			cout << "It's super effective!" << endl;
		}
	}

	else if (this->type == Bug)
	{
		if (enemyPokemon->type == Fighting || enemyPokemon->type == Fire || enemyPokemon->type == Poison || enemyPokemon->type == Steel || enemyPokemon->type == Ghost || enemyPokemon->type == Flying)
		{
			this->currentDamage = this->currentDamage / 2;
			cout << "It's not very effective..." << endl;
		}

		else if (enemyPokemon->type == Grass || enemyPokemon->type == Psychic || enemyPokemon->type == Dark)
		{
			this->currentDamage *= 2;
			cout << "It's super effective!" << endl;
		}
	}

	else if (this->type == Flying)
	{
		if (enemyPokemon->type == Electric || enemyPokemon->type == Rock || enemyPokemon->type == Steel)
		{
			this->currentDamage = this->currentDamage / 2;
			cout << "It's not very effective..." << endl;
		}

		else if (enemyPokemon->type == Grass || enemyPokemon->type == Fighting || enemyPokemon->type == Bug)
		{
			this->currentDamage *= 2;
			cout << "It's super effective!" << endl;
		}
	}

	else if (this->type == Normal)
	{
		if (enemyPokemon->type == Rock || enemyPokemon->type == Steel)
		{
			this->currentDamage = this->currentDamage / 2;
			cout << "It's not very effective..." << endl;
		}

		else if (enemyPokemon->type == Ghost)
		{
			this->currentDamage *= 0;
			cout << "It has no effect..." << endl;
		}
	}

	else if (this->type == Poison)
	{
		if (enemyPokemon->type == Poison || enemyPokemon->type == Rock || enemyPokemon->type == Ground || enemyPokemon->type == Ghost)
		{
			this->currentDamage = this->currentDamage / 2;
			cout << "It's not very effective..." << endl;
		}

		else if (enemyPokemon->type == Grass)
		{
			this->currentDamage *= 2;
			cout << "It's super effective!" << endl;
		}

		else if (enemyPokemon->type == Steel)
		{
			this->currentDamage *= 0;
			cout << "It has no effect..." << endl;
		}
	}

	else if (this->type == Psychic)
	{
		if (enemyPokemon->type == Psychic || enemyPokemon->type == Steel)
		{
			this->currentDamage = this->currentDamage / 2;
			cout << "It's not very effective..." << endl;
		}

		else if (enemyPokemon->type == Fighting || enemyPokemon->type == Poison)
		{
			this->currentDamage *= 2;
			cout << "It's super effective!" << endl;
		}

		else if (enemyPokemon->type == Dark)
		{
			this->currentDamage *= 0;
			cout << "It has no effect..." << endl;
		}
	}

	else if (this->type == Ghost)
	{
		if (enemyPokemon->type == Dark)
		{
			this->currentDamage = this->currentDamage / 2;
			cout << "It's not very effective..." << endl;
		}

		else if (enemyPokemon->type == Ghost || enemyPokemon->type == Psychic)
		{
			this->currentDamage *= 2;
			cout << "It's super effective!" << endl;
		}

		else if (enemyPokemon->type == Normal)
		{
			this->currentDamage *= 0;
			cout << "It has no effect..." << endl;
		}
	}

	else if (this->type == Fighting)
	{
		if (enemyPokemon->type == Poison || enemyPokemon->type == Flying || enemyPokemon->type == Psychic || enemyPokemon->type == Bug)
		{
			this->currentDamage = this->currentDamage / 2;
			cout << "It's not very effective..." << endl;
		}

		else if (enemyPokemon->type == Normal || enemyPokemon->type == Ice || enemyPokemon->type == Rock || enemyPokemon->type == Dark || enemyPokemon->type == Steel)
		{
			this->currentDamage *= 2;
			cout << "It's super effective!" << endl;
		}

		else if (enemyPokemon->type == Ghost)
		{
			this->currentDamage *= 0;
			cout << "It has no effect..." << endl;
		}
	}

	else if (this->type == Dark)
	{
		if (enemyPokemon->type == Dark || enemyPokemon->type == Fighting)
		{
			this->currentDamage = this->currentDamage / 2;
			cout << "It's not very effective..." << endl;
		}

		else if (enemyPokemon->type == Ghost || enemyPokemon->type == Psychic)
		{
			this->currentDamage *= 2;
			cout << "It's super effective!" << endl;
		}
	}

	else if (this->type == Steel)
	{
		if (enemyPokemon->type == Fire || enemyPokemon->type == Water || enemyPokemon->type == Electric || enemyPokemon->type == Steel)
		{
			this->currentDamage = this->currentDamage / 2;
			cout << "It's not very effective..." << endl;
		}

		else if (enemyPokemon->type == Ice || enemyPokemon->type == Rock)
		{
			this->currentDamage *= 2;
			cout << "It's super effective!" << endl;
		}
	}
}

