#include "ItemList.h"

ItemList::ItemList()
{
	MegaStone* Sceptilite = new MegaStone("Sceptilite", "Sceptile", 1, 500);
	MegaStone* Aggronite = new MegaStone("Aggronite", "Aggron", 1, 500);
	MegaStone* Metagrossite = new MegaStone("Metagrossite", "Metagross", 1, 500);
	MegaStone* Blazikenite = new MegaStone("Blazikenite", "Blaziken", 1, 500);
	MegaStone* Swampertite = new MegaStone("Swampertite", "Swampert", 1, 500);

	Pokeball* pokeBall = new Pokeball("Pokeball", 30, 200, 5); //player starts with 5 playerPokeBalls
	Pokeball* greatBall = new Pokeball("Great Ball", 45, 600, 0);
	Pokeball* ultraBall = new Pokeball("Ultra Ball", 60, 1200, 0);


	this->megStoneList.push_back(Sceptilite);
	this->megStoneList.push_back(Aggronite);
	this->megStoneList.push_back(Metagrossite);
	this->megStoneList.push_back(Blazikenite);
	this->megStoneList.push_back(Swampertite);

	this->pokeballList.push_back(pokeBall);
	this->pokeballList.push_back(greatBall);
	this->pokeballList.push_back(ultraBall);
}
