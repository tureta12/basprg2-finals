#include "Player.h"
#include <algorithm>

Player::Player()
{

}

void Player::Walk(Map* map)
{
	system("cls");
	this->displayCurrentCoordinates();

	if (this->inBounds == false)
	{
		cout << "You are out of bounds. Please go back.\n" << endl;
	}

	else
	{
		cout << "You are in " << this->currentLocation->name << "\n" << endl;
	}

	this->movementChoice();
	this->updateCurrentLocation(map->locationList, this->inBounds);
	/*this->displayCurrentCoordinates();
	cout << this->currentLocation->name;*/
}

void Player::moveUp()
{
	this->yCoordinate++;
}

void Player::moveLeft()
{
	this->xCoordinate--;
}

void Player::moveRight()
{
	this->xCoordinate++;
}

void Player::moveDown()
{
	this->yCoordinate--;
}

void Player::displayCurrentCoordinates()
{
	cout << "You are now at (" << this->xCoordinate << ", " << this->yCoordinate << ")" << endl;
}

void Player::loopTillValidInput(int &input) //for integer only 
{
	while (!(cin >> input))
	{
		cout << "Please enter a valid choice." << endl;
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
	}
}

void Player::displayInventory()
{
	system("cls");
	cout << "Please select a category." << endl;
	this->playerInventory->displayInventoryCategories();
	int inventoryChoice = 0;

	while (true)
	{
		this->loopTillValidInput(inventoryChoice);

		if (inventoryChoice == 1)
		{
			this->playerInventory->displayplayerPokeBalls();
			system("pause");
			break;
		}

		else if (inventoryChoice == 2)
		{
			this->playerInventory->displayplayerMegaStones();
			system("pause");
			break;
		}

		else if (inventoryChoice = 3) //rare candies
		{
			int rareCandyChoice = 0;
			cout << "Rare Candy " << this->playerInventory->playerRareCandies->count << "x\n" << endl;
			cout << "Would you like to use a Rare Candy on a Pokemon?" << endl;
			cout << " [1] Yes     [2] No" << endl;
			while (true)
			{
				loopTillValidInput(rareCandyChoice);

				if (rareCandyChoice == 1)
				{
					if (this->playerInventory->playerRareCandies->count == 0)
					{
						cout << "Oh, you don't have anymore rare candies." << endl;
					}

					else
					{
						this->useRareCandy();
					}

					break;
				}

				else if (rareCandyChoice == 2)
				{
					//nothing, will exit
					break;
				}

				else
				{
					cout << "Please enter a valid choice." << endl;
				}
			}
			break;
		}

		else if (inventoryChoice == 0)
		{
			break;//do nothing
		}

		else
		{
			cout << "Please enter a valid input." << endl;
		}
	}
}

void Player::playerMenuDecision(int & choice)
{
	if (this->currentLocation->hasPokemonCenter == false || this->inBounds == false)
	{
		cout << "What do you want to do?" << endl;
		cout << "[1] Walk     [2] Pokemon     [3] Bag     [4] Stats     [7] Exit" << endl; //currency is in stats
	}

	else if (this->currentLocation->hasPokemonCenter == true)
	{
		cout << "What do you want to do?" << endl;
		cout << "[1] Walk     [2] Pokemon     [3] Bag     [4] Stats     [5] Pokemon Center     [6] Pokemart     [7] Exit" << endl; //currency is in stats
	}
	loopTillValidInput(choice);
}

void Player::movementChoice()
{
	cout << "Where do you want to go?" << endl;
	cout << "[W] Up          [A] Left         [S] Down        [D] Right" << endl;

	string movementChoice;
	while (true)
	{
		getline(cin, movementChoice);
		if (movementChoice == "W" || movementChoice == "w")
		{
			this->moveUp();
			break;
		}
		else if (movementChoice == "A" || movementChoice == "a")
		{
			this->moveLeft();
			break;
		}
		else if (movementChoice == "S" || movementChoice == "s")
		{
			this->moveDown();
			break;
		}
		else if (movementChoice == "D" || movementChoice == "d")
		{
			this->moveRight();
			break;
		}
		else
		{
			cout << "Please input a valid choice." << endl;

		}
	}
}

void Player::updateCurrentLocation(vector<Location*> locationList, bool &inBounds) //checks all locations until it's found a location
{
	for (unsigned int i = 0; i < locationList.size(); i++)
	{
		//where j=0 is x and j=1 is y
		int j = 0;
		bool inX = false, inY = false;
		if (this->xCoordinate >= locationList[i]->bottomLeftCoordinates[j] && this->xCoordinate <= locationList[i]->topRightCoordinates[j])
		{
			inY = true;
		}

		j = 1;
		if (this->yCoordinate >= locationList[i]->bottomLeftCoordinates[j] && this->yCoordinate <= locationList[i]->topRightCoordinates[j])
		{
			inX = true;
		}
		if (inX == true && inY == true)
		{
			inBounds = true;
			this->currentLocation = locationList[i]; //update player location
			break;
		}
		else
		{
			inBounds = false;
		}
	}
}

void Player::addPokemon(Pokemon* pokemon)
{
	this->playerPokemons.push_back(pokemon);
}

Pokemon * Player::getPokemon(int playerChoice)
{
	return this->playerPokemons.at(playerChoice - 1); //ok?
}

void Player::displayPlayerPokemon()
{
	cout << "\n" << endl;
	for (unsigned int i = 0; i < this->playerPokemons.size(); i++)
	{
		Pokemon* current = this->playerPokemons[i]; // for less typing?

		cout << current->name << endl;
		this->playerPokemons[i]->printType(); //cuz enums 
		cout << " Type" << endl;												 //Type
		cout << "Level " << current->level << endl; //for less typing?			 //Level
		if (current->currentHP < 0)
		{
			cout << "HP: 0/ " << current->baseHP << endl;
		}
		else
		{
			cout << "HP: " << current->currentHP << "/" << current->baseHP << endl;  //HP
		}
		cout << "Damage: " << current->baseDamage << endl;						 //Damage
		cout << "XP: " << current->exp << "/" << current->baseExpToNextLevel << endl << endl;  //XP
	}
}

void Player::removePokemon(Pokemon* pokemon)
{
	this->playerPokemons.erase(remove(this->playerPokemons.begin(), this->playerPokemons.end(), pokemon), this->playerPokemons.end()); //to delete a specificz pokemonz. just in case I implement a system, pokemon center PC
	//but you must then transfer this pokemon to the PC. so i guess transfer first (copy) then remove.
}

void Player::healPokemon()
{
	system("cls");
	cout << "Hello, welcome to the PokemonCenter, would you like me to heal your Pokemon?" << endl;
	cout << "[1] Yes        [2] No" << endl;
	int healPokemonChoice;

	while (true)
	{
		loopTillValidInput(healPokemonChoice);
		if (healPokemonChoice == 1)
		{
			cout << "Ten ten teneten" << endl;
			system("pause");
			cout << "Your Pokemon are now in full health, please come again!" << endl;
			//go through all of player's pokemon and equate the currentHP to the base HP
			for (unsigned int i = 0; i < this->playerPokemons.size(); i++)
			{
				this->playerPokemons[i]->currentHP = this->playerPokemons[i]->baseHP;
			}
			system("pause");
			break;
		}

		else if (healPokemonChoice == 2)
		{
			cout << "Oh ok, please come again!" << endl;
			system("pause");
			break;
		}

		else
		{
			cout << "Please enter a valid choice" << endl;
		}
	}

}

void Player::openOpenPokeMart()
{
	system("cls");
	cout << "Welcome to the Pokemart!" << endl;
	cout << "What would you like to buy?" << endl;
	cout << "===============================================" << endl;
	this->playerInventory->displayInventoryCategories();

	int categoryChoice = 0, buyChoice = 0;
	while (true)
	{
		this->loopTillValidInput(categoryChoice);

		if (categoryChoice == 1)
		{
			cout << endl << "Please select an item:" << endl;
			this->playerInventory->displayplayerPokeBallsPrice();

			while (true)
			{
				loopTillValidInput(buyChoice);
				if (buyChoice >= 0 && buyChoice <= this->playerInventory->playerPokeBalls.size())
				{
					break;
				}
				cout << "Please enter a valid choice." << endl;
			}

			if (buyChoice == 0)
			{

			}
			else 			//PURCHASED
			{
				buyChoice--; //cause vectors start at 0
				if (this->hasEnoughPoke(this->playerInventory->playerPokeBalls[buyChoice]->price) == false)
				{
					cout << "You don't have enough Poke." << endl;
					system("pause");
				}

				else
				{
					cout << "You have purchased a " << this->playerInventory->playerPokeBalls[buyChoice]->name << "!" << endl;
					cout << "You handed over " << this->playerInventory->playerPokeBalls[buyChoice]->price << "." << endl << endl;
					this->currency -= this->playerInventory->playerPokeBalls[buyChoice]->price;
					this->playerInventory->playerPokeBalls[buyChoice]->count++;
					cout << endl;
					system("pause");
				}
			}
			break;
		}

		else if (categoryChoice == 2)
		{
			cout << endl << "Please select an item:" << endl;
			this->playerInventory->displayplayerMegaStonesPrice();
			while (true)
			{
				loopTillValidInput(buyChoice);
				if (buyChoice >= 0 && buyChoice <= this->playerInventory->playerMegaStones.size())
				{
					break;
				}
				cout << "Please enter a valid choice." << endl;
			}

			if (buyChoice == 0)
			{

			}

			else 			//PURCHASED
			{
				buyChoice--; //cause vectors start at 0
				if (this->hasEnoughPoke(this->playerInventory->playerMegaStones[buyChoice]->price) == false)
				{
					cout << "You don't have enough Poke." << endl;
					system("pause");
				}

				else
				{
					cout << "You have purchased a " << this->playerInventory->playerMegaStones[buyChoice]->name << "!" << endl;
					cout << "You handed over " << this->playerInventory->playerMegaStones[buyChoice]->price << "." << endl << endl;
					this->currency -= this->playerInventory->playerMegaStones[buyChoice]->price;
					this->playerInventory->playerMegaStones[buyChoice]->count++;
					cout << endl;
					system("pause");
				}
			}
			break;
		}

		else if (categoryChoice == 3)
		{
			cout << endl << "Please select an item: " << endl;
			cout << "[1] Rare Candy: " << this->playerInventory->playerRareCandies->price << " Poke" << "      [0] Exit\n" << endl;

			while (true)
			{
				loopTillValidInput(buyChoice);
				if (buyChoice == 1)
				{
					if (this->hasEnoughPoke(this->playerInventory->playerRareCandies->price) == false)
					{
						cout << "You don't have enough Poke." << endl;
						system("pause");
					}

					else
					{
						cout << endl << "You've bought a Rare Candy!" << endl;
						cout << "You handed over " << this->playerInventory->playerRareCandies->price << "." << endl;
						this->currency -= this->playerInventory->playerRareCandies->price;
						this->playerInventory->playerRareCandies->count++;
						cout << endl;
						system("pause");
					}
					break;
				}

				else if (buyChoice == 0)
				{
					//nothing
					break;
				}

				else
				{
					cout << "Please enter a valid choice." << endl;
				}
			}
			break;
		}
		else if (categoryChoice == 0)
		{
			//nothing. will exit
			break;
		}

		else
		{
			cout << "Please enter a valid input." << endl;
		}
	}
}

void Player::displayStats()
{
	system("cls");
	cout << "Your current Poke: " << this->currency << endl;
	cout << endl;
}

void Player::wildPokemonEncounter(Pokedex* pokedex)
{
	system("cls");
	this->displayCurrentCoordinates();

	if (this->inBounds == false)
	{
		cout << "You are out of bounds. Please go back.\n" << endl;
	}

	else
	{
		cout << "You are in " << this->currentLocation->name << "\n" << endl;
	}

	Pokemon* wildPokemon = new Pokemon();

	*wildPokemon = *pokedex->spawnPokemon(); //we now have a wild pokemon

	//time to give him a random level from 1 - 10 and apply appropriate stats
	float wildPokemonLevel = (rand() % 10) + 1;
	wildPokemon->levelUp(wildPokemonLevel - 1); //-1 cuz a default pokemon is lvl 1.

	cout << "You've encountered a wild Pokemon!" << endl << endl;
	cout << "A wild " << wildPokemon->name << " has appeared!\n" << endl;
	wildPokemon->displayStats();

	this->battlingPokemon = this->returnCurrentPokemon();

	if (battlingPokemon->currentHP <= 0) //YOU CAN'T FIGHT WITH NO POKEMON
	{
		cout << "Oh no, you are out of useable Pokemon!" << endl;
		cout << "You ran away!\n" << endl;
		system("pause");
	}

	else //BATTLE
	{
		cout << "Go " << battlingPokemon->name << "!" << endl;
		bool inBattle = true;
		system("pause");

		while (inBattle)
		{
			system("cls");
			wildPokemon->displayStats();
			cout << endl << "what would you want to do?" << endl;
			cout << " [1] Fight     [2] Pokemon     [3] Bag     [4] Run" << endl;

			bool inEncounter = true;
			while (inEncounter)
			{
				int encounterChoice = 0;
				loopTillValidInput(encounterChoice);

				if (encounterChoice == 1)
				{
					this->encounterFight(wildPokemon, inBattle);
					break;
				}

				else if (encounterChoice == 2)
				{
					system("cls");
					this->displayPlayerPokemon();
					system("pause");
					break;
				}

				else if (encounterChoice == 3)
				{
					this->openBag(wildPokemon, inBattle);
					break;
				}

				else if (encounterChoice == 4)
				{
					cout << "You ran away safely!" << endl;
					system("pause");
					inBattle = false;
					break;
				}

				else
				{
					cout << "Please enter a valid input." << endl;
				}
			}
		}
		this->deMegaEvolvePokemon(this->evolutionBeforeMega);//make a devolve function which loops through all of the player's pokemons. to check if any have been mega evolvd. we can then make use of the copy of the vector above.
	}
}

void Player::encounterFight(Pokemon* wildPokemon, bool& inBattle)
{
	battlingPokemon->attack(wildPokemon);
	cout << endl;
	system("pause");
	cout << endl;
	if (wildPokemon->currentHP <= 0)
	{
		cout << wildPokemon->name << " has been defeated!" << endl << endl;
		battlingPokemon->exp += wildPokemon->givenXP;
		cout << battlingPokemon->name << " has gained " << wildPokemon->givenXP << " XP!\n" << endl;
		system("pause");
		//levelup Check
		if (battlingPokemon->exp >= battlingPokemon->baseExpToNextLevel)
		{
			//lvl up!
			cout << battlingPokemon->name << " has levelled up!" << endl;
			battlingPokemon->levelUp(1);
			cout << endl << endl;
			system("pause");
			battlingPokemon->evolutionCheck(); //just like in the game, only EVOLVE when level up occurs
		}

		inBattle = false;
	}

	else
	{
		wildPokemon->attack(battlingPokemon);
		cout << endl << endl;
		system("pause");
		cout << endl;
		if (battlingPokemon->currentHP <= 0) //if you pokemon is defeated, "Bring out" the next one. 
		{
			cout << battlingPokemon->name << " has been defeated!" << endl;
			cout << "You bring out your next Pokemon.\n" << endl;
			system("pause");

			battlingPokemon = this->returnCurrentPokemon();

			if (battlingPokemon->currentHP <= 0)
			{
				cout << "You are out of useable Pokemon!" << endl;
				system("pause");
				cout << "You ran away!\n" << endl;
				system("pause");
				inBattle = false;
			}

			else
			{
				cout << "Go " << battlingPokemon->name << "!" << endl;
				system("pause");
			}
		}
	}
}

void Player::openBag(Pokemon* wildPokemon, bool &inBattle)
{
	int categoryChoice = 0, itemChoice = 0;
	system("cls");
	cout << "Choose a cateogry." << endl;
	this->playerInventory->displayInventoryCategories();

	while (true)
	{
		loopTillValidInput(categoryChoice);

		if (categoryChoice == 1) //Pokeballs
		{
			system("cls");
			cout << "Choose a Pokeball to catch a Pokemon with." << endl;
			this->playerInventory->displayplayerPokeBallsBattle();
			while (true)
			{
				loopTillValidInput(itemChoice);

				if (itemChoice == 0)
				{
					//nothing
					break;
				}
				else if (itemChoice == 1 || itemChoice == 2 || itemChoice == 3)
				{
					this->usePokeball(itemChoice, wildPokemon, inBattle);
					break;
				}
				else
				{
					cout << "Please enter a valid input." << endl;
				}
			}
			break;
		}

		else if (categoryChoice == 2) //Megastones
		{
			system("cls");
			cout << "Choose a Megastone to use." << endl;
			this->playerInventory->displayplayerMegaStonesBattle();
			while (true)
			{
				loopTillValidInput(itemChoice);

				if (itemChoice == 0)
				{
					//nothing
					break;
				}
				else if (itemChoice > 0 && itemChoice <= 5)
				{
					this->useMegastone(itemChoice, this->battlingPokemon);
					break;
				}
				else
				{
					cout << "Please enter a valid input." << endl;
				}
			}
			break;
		}

		else if (categoryChoice == 3) //rare candies
		{
			cout << "Can't use that here!" << endl;
		}

		else if (categoryChoice == 0)
		{
			//nothing
			break;
		}
		else
		{
			cout << "Please enter a valid input." << endl;
		}
	}
}

void Player::usePokeball(int choice, Pokemon* wildPokemon, bool &inBattle)
{
	int pokemonCatchRoll = 0;
	choice--; //vectors start at 0
	system("cls");
	if (this->playerInventory->playerPokeBalls[choice]->count == 0)
	{
		cout << "Oops, you're out of this Pokeball type." << endl;
		system("pause");
	}

	else
	{
		cout << "You threw a " << this->playerInventory->playerPokeBalls[choice]->name << "!" << endl;
		this->playerInventory->playerPokeBalls[choice]->count--;

		system("pause");
		cout << "." << endl;
		system("pause");
		cout << ".." << endl;
		system("pause");
		cout << "..." << endl;

		pokemonCatchRoll = rand() % 100 + 1;
		if (pokemonCatchRoll >= this->playerInventory->playerPokeBalls[choice]->catchChance) //caught a pokemon
		{
			cout << "The wild " << wildPokemon->name << " was caught!\n" << endl << endl;
			cout << wildPokemon->name << " has been added to your Pokemon." << endl << endl;
			wildPokemon->displayStats();
			this->playerPokemons.push_back(wildPokemon);
			inBattle = false;
			system("pause");
		}

		else
		{
			cout << "The wild " << wildPokemon->name << " got out of the ball!\n" << endl;
			system("pause");
		}
	}
}

void Player::useMegastone(int choice, Pokemon* battlingPokemon) //my code only accomodates the current pokemon out
{
	system("cls");
	choice--;
	cout << "You give the " << this->playerInventory->playerMegaStones[choice]->name << " to " << battlingPokemon->name << "." << endl;
	if (this->playerInventory->playerMegaStones[choice]->compatiblePokemonName == battlingPokemon->name)
	{
		battlingPokemon->megaEvolved = true;
		*this->evolutionBeforeMega = *battlingPokemon;
		//Pokemon* temporary = this->playerPokemons[currentPokemonPos];//make a copy 
		battlingPokemon->Evolve();
	}
	else
	{
		cout << "It doesn't seem to have any effect." << endl;
		system("pause");
	}
}

void Player::useRareCandy()
{
	int pokemonToGiveCandy = 0;

	system("cls");
	cout << "Please select a Pokemon to give a rare candy to.\n" << endl;
	for (unsigned int i = 0; i < this->playerPokemons.size(); i++)
	{
		cout << "[" << i + 1 << "] " << this->playerPokemons[i]->name << endl << endl;
	}
	cout << "[0] Exit" << endl;
	while (true)
	{
		this->loopTillValidInput(pokemonToGiveCandy);

		if (pokemonToGiveCandy < 0 || pokemonToGiveCandy > this->playerPokemons.size())
		{
			cout << "Please enter a valid input." << endl;
		}

		else if (pokemonToGiveCandy == 0)
		{
			break; //exit
		}

		else
		{
			this->playerInventory->playerRareCandies->count--;

			//level up function
			cout << this->playerPokemons[pokemonToGiveCandy - 1]->name << " has levelled up!" << endl << endl;
			this->playerPokemons[pokemonToGiveCandy - 1]->levelUp(1);
			system("pause");
			cout << endl;
			this->playerPokemons[pokemonToGiveCandy - 1]->evolutionCheck();
			break;
		}
	}
}

bool Player::hasEnoughPoke(int itemPrice)
{
	if (this->currency - itemPrice < 0)
	{
		return false;
	}

	else
	{
		return true;
	}
}

Pokemon* Player::returnCurrentPokemon() //cycles through the player's pokemon list till it comes upon a pokemon whose hp is greater than 0
{
	int positionOfCurrentPokemon = 0;
	for (unsigned int i = 0; i < this->playerPokemons.size(); i++)
	{
		if (this->playerPokemons[i]->currentHP > 0)
		{
			positionOfCurrentPokemon = i;
			break;
		}
	}
	return this->playerPokemons[positionOfCurrentPokemon];
}

void Player::deMegaEvolvePokemon(Pokemon* beforeMega) //cycle through player's pokemon list, looking for a mega evolved pokemon. when it finds one, revert it to its previous stats DISREGARDING XP.
{
	for (unsigned int i = 0; i < this->playerPokemons.size(); i++)
	{
		if (this->playerPokemons[i]->megaEvolved == true) //meaning to say it has mega evolved. 
		{
			cout << endl << this->playerPokemons[i]->name << " has reverted to its original form." << endl;
			system("pause");
			int xpAfterBattle = this->playerPokemons[i]->exp; //hold the xp after the encounter
			*this->playerPokemons[i] = beforeMega[i];
			this->playerPokemons[i]->exp = xpAfterBattle; //so that XP will not revert
			this->playerPokemons[i]->megaEvolved = false;
		}
	}
}
