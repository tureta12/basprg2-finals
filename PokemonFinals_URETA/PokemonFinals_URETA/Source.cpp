
#include "GameManager.h"

int main()
{
	Player* myPlayer = new Player();
	Map* map = new Map();
	GameManager* manager = new GameManager();

	manager->startGame(myPlayer, map);
	return 0;
}