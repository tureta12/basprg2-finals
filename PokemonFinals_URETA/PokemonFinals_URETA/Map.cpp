#include "Map.h"

Map::Map()
{
	//LOCATIONS
	Location* LittleRootTown = new Location("Littleroot Town", true, /*topLEFT*/{ -2,2 }, /*bottomLEFT*/{ -2,-2 }, /*topRight*/{ 2,2 }, /*bottomRIGHT*/{ 2,-2 });
	Location* Route01 = new Location("Route 01", false,              /*topLEFT*/{ -2,7 }, /*bottomLEFT*/{ -2,3 }, /*topRight*/{ 2,7 }, /*bottomRIGHT*/{ 2, 3 });
	Location* OldaleTown = new Location("Oldale Town", true,         /*topLEFT*/{ -3,12 }, /*bottomLEFT*/{ -3,8 }, /*topRight*/{ 3,12 }, /*bottomRIGHT*/{ 3, 8 });
	Location* Route02 = new Location("Route 02", false,              /*topLEFT*/{ -13,14 }, /*bottomLEFT*/{ -13,8 }, /*topRight*/{ -4,14 }, /*bottomRIGHT*/{ -4, 8 });
	Location* Route54 = new Location("Route 54", false,              /*topLEFT*/{ -3,17 }, /*bottomLEFT*/{ -3,13 }, /*topRight*/{ 3,17 }, /*bottomRIGHT*/{ 3, 13 });
	Location* PetalburgCity = new Location("Petalburg City", true,   /*topLEFT*/{ -17,16 }, /*bottomLEFT*/{ -17,10 }, /*topRight*/{ -14,16 }, /*bottomRIGHT*/{ -14, 10 });

	locationList = { LittleRootTown, Route01, OldaleTown, Route02, Route54, PetalburgCity };
}
