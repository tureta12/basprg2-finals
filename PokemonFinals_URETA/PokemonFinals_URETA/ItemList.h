#pragma once
#include "Pokeball.h"
#include "Megastone.h"
#include "RareCandy.h"
#include <vector>

class ItemList
{
public:
	ItemList();
	vector<Pokeball*> pokeballList;
	vector<MegaStone*> megStoneList;
	RareCandy* rareCandy = new RareCandy();
};