#pragma once
#include "Inventory.h"
#include "Map.h"
#include "Pokemon.h"
#include "Pokedex.h"

#include <string>
#include <limits>
#include <iostream>

class Player
{
public:
	Player(); //default constructor

	string name;
	int currency = 10000;
	int xCoordinate = 0;
	int yCoordinate = 0;
	bool inBounds = true;

	Inventory* playerInventory = new Inventory();
	Location* currentLocation;
	vector<Pokemon*> playerPokemons;
	Pokemon* battlingPokemon = new Pokemon();
	Pokemon* evolutionBeforeMega = new Pokemon();

	//movement functions
	void Walk(Map*);
	void moveUp();
	void moveLeft();
	void moveRight();
	void moveDown();

	//map related
	void displayCurrentCoordinates();
	void updateCurrentLocation(vector<Location*>, bool&);

	//functions connected to the list of player Pokemon
	void addPokemon(Pokemon*);
	Pokemon* getPokemon(int); //returns the pokemon at the specified index
	void displayPlayerPokemon(); //prints all player's pokemon with stats
	void removePokemon(Pokemon*); // might not use this. 

	//PokemonCenter and PokeMart
	void healPokemon();
	void openOpenPokeMart();

	//Display Stats which is just currency lol
	void displayStats();

	//Encounter
	void wildPokemonEncounter(Pokedex*);

	//input checker
	void loopTillValidInput(int&);

	//others
	void encounterFight(Pokemon*, bool&);
	void displayInventory();
	void playerMenuDecision(int &choice);
	void movementChoice();
	void openBag(Pokemon*, bool&);
	void usePokeball(int, Pokemon*, bool&);
	void useMegastone(int, Pokemon*);
	void useRareCandy();
	bool hasEnoughPoke(int);
	Pokemon* returnCurrentPokemon();
	void deMegaEvolvePokemon(Pokemon*);
};