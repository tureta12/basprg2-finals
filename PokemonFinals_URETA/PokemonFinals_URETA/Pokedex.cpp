#include "Pokedex.h"

Pokedex::Pokedex()
{

	//POKEMON
	Pokemon* Crobat = new Pokemon("Crobat", /*baseHP*/40,/*LEVEL*/16,/*baseDamage*/45,/*xpToNextLevel*/179,/*GivenXP*/141, Bug,/*levelToEvolve*/1000,/*Evolved Form*/ NULL);
	Pokemon* Golbat = new Pokemon("Golbat", /*baseHP*/40,/*LEVEL*/16,/*baseDamage*/45,/*xpToNextLevel*/179, /*GivenXP*/ 141, Bug,/*levelToEvolve*/ 36, /*Evolved Form*/ Crobat);
	Pokemon* Zubat = new Pokemon("Zubat",/*baseHP*/11,/*LEVEL*/1,/*baseDamage*/6,/*xpToNextLevel*/18, /*GivenXP*/ 8, Bug,/*levelToEvolve*/ 20, /*Evolved Form*/ Golbat);

	Pokemon* Mightyena = new Pokemon("Mightyena",/*baseHP*/40,/*LEVEL*/16,/*baseDamage*/45,  /*xpToNextLevel*/179, /*GivenXP*/ 141, Normal,/*levelToEvolve*/ 1000, /*Evolved Form*/ NULL);
	Pokemon* Poochyena = new Pokemon("Poochyena", /*baseHP*/13,/*LEVEL*/1,/*baseDamage*/8,  /*xpToNextLevel*/19, /*GivenXP*/ 9, Normal,/*levelToEvolve*/ 20, /*Evolved Form*/ Mightyena);

	Pokemon* Linoone = new Pokemon("Linoone", /*baseHP*/40,/*LEVEL*/16,/*baseDamage*/45, /*xpToNextLevel*/179, /*GivenXP*/ 141, Normal,/*levelToEvolve*/ 1000, /*Evolved Form*/ NULL);
	Pokemon* Zigzagoon = new Pokemon("Zigzagoon", /*baseHP*/10,/*LEVEL*/1,/*baseDamage*/5,  /*xpToNextLevel*/20, /*GivenXP*/ 7, Normal,/*levelToEvolve*/ 20, /*Evolved Form*/ Linoone);

	Pokemon* Plusle = new Pokemon("Plusle",/*baseHP*/12,/*LEVEL*/1,/*baseDamage*/7, /*xpToNextLevel*/18, /*GivenXP*/ 9, Normal,/*levelToEvolve*/ 1000, /*Evolved Form*/ NULL);

	Pokemon* Breloom = new Pokemon("Breloom",/*baseHP*/40,/*LEVEL*/16,/*baseDamage*/45, /*xpToNextLevel*/179, /*GivenXP*/ 141, Grass,/*levelToEvolve*/ 1000, /*Evolved Form*/ NULL);
	Pokemon* Shroomish = new Pokemon("Shroomish",/*baseHP*/11,/*LEVEL*/1,/*baseDamage*/6, /*xpToNextLevel*/19, /*GivenXP*/ 10, Grass,/*levelToEvolve*/ 26, /*Evolved Form*/ Breloom);

	Pokemon* Swellow = new Pokemon("Swellow",/*baseHP*/40,/*LEVEL*/16,/*baseDamage*/45, /*xpToNextLevel*/179, /*GivenXP*/ 141, Flying,/*levelToEvolve*/ 1000, /*Evolved Form*/ NULL);
	Pokemon* Tailow = new Pokemon("Tailow", /*baseHP*/11,/*LEVEL*/1,/*baseDamage*/6, /*xpToNextLevel*/17, /*GivenXP*/ 9, Flying,/*levelToEvolve*/ 22, /*Evolved Form*/ Swellow);

	Pokemon* Gardevoir = new Pokemon("Gardevoir",/*baseHP*/40,/*LEVEL*/16,/*baseDamage*/45,  /*xpToNextLevel*/179, /*GivenXP*/ 141, Psychic,/*levelToEvolve*/ 1000, /*Evolved Form*/ NULL);
	Pokemon* Kirlia = new Pokemon("Kirlia",/*baseHP*/40,/*LEVEL*/16,/*baseDamage*/45,/*xpToNextLevel*/179, /*GivenXP*/ 141, Psychic,/*levelToEvolve*/ 36, /*Evolved Form*/ Gardevoir);
	Pokemon* Ralts = new Pokemon("Ralts", /*baseHP*/10,/*LEVEL*/1,/*baseDamage*/9, /*xpToNextLevel*/18, /*GivenXP*/ 8, Psychic,/*levelToEvolve*/ 22, /*Evolved Form*/ Kirlia);

	Pokemon* Golem = new Pokemon("Golem",/*baseHP*/40,/*LEVEL*/16,/*baseDamage*/45, /*xpToNextLevel*/179, /*GivenXP*/ 141, Rock,/*levelToEvolve*/ 1000, /*Evolved Form*/ NULL);
	Pokemon* Graveler = new Pokemon("Graveler",/*baseHP*/40,/*LEVEL*/16,/*baseDamage*/45, /*xpToNextLevel*/179, /*GivenXP*/ 141, Rock,/*levelToEvolve*/ 36, /*Evolved Form*/ Golem);
	Pokemon* Geodude = new Pokemon("Geodude",/*baseHP*/15,/*LEVEL*/1,/*baseDamage*/6, /*xpToNextLevel*/19, /*GivenXP*/ 10, Rock,/*levelToEvolve*/ 23, /*Evolved Form*/ Graveler);

	Pokemon* Vileplume = new Pokemon("Vileplume",/*baseHP*/40,/*LEVEL*/16,/*baseDamage*/45,/*xpToNextLevel*/179, /*GivenXP*/ 141, Grass,/*levelToEvolve*/ 1000,/*Evolved Form*/ NULL);
	Pokemon* Oddish = new Pokemon("Oddish",/*baseHP*/11,/*LEVEL*/1,/*baseDamage*/7, /*xpToNextLevel*/17, /*GivenXP*/81, Grass,/*levelToEvolve*/ 23, /*Evolved Form*/ Vileplume);

	Pokemon* Manectric = new Pokemon("Manectric",/*baseHP*/40,/*LEVEL*/16,/*baseDamage*/45,/*xpToNextLevel*/179, /*GivenXP*/ 141, Electric,/*levelToEvolve*/ 1000, /*Evolved Form*/ NULL);
	Pokemon* Electrike = new Pokemon("Electrike",/*baseHP*/7,/*LEVEL*/1,/*baseDamage*/5, /*xpToNextLevel*/ 17, /*GivenXP*/ 7, Electric,/*levelToEvolve*/ 32, /*Evolved Form*/ Manectric);

	Pokemon* MegaSceptile = new Pokemon("Mega-Sceptile",/*baseHP*/40,/*LEVEL*/16,/*baseDamage*/45, /*xpToNextLevel*/179, /*GivenXP*/ 141, Grass,/*levelToEvolve*/ 1000, /*Evolved Form*/ NULL);
	Pokemon* Sceptile = new Pokemon("Sceptile",/*baseHP*/40,/*LEVEL*/36,/*baseDamage*/45, /*xpToNextLevel*/44, /*GivenXP*/ 208, Grass,/*levelToEvolve*/ 36, /*Evolved Form*/ MegaSceptile);
	Pokemon* Grovyle = new Pokemon("Grovlye",/*baseHP*/40,/*LEVEL*/16,/*baseDamage*/45, /*xpToNextLevel*/179, /*GivenXP*/ 141, Grass,/*levelToEvolve*/ 36, /*Evolved Form*/ Sceptile);
	Pokemon* Treecko = new Pokemon("Treecko",/*baseHP*/14,/*LEVEL*/1,/*baseDamage*/7,  /*xpToNextLevel*/20, /*GivenXP*/ 10, Grass, /*levelToEvolve*/ 16, /*Evolved Form*/ Grovyle);

	Pokemon* MegaSwampert = new Pokemon("Mega-Swampert",/*baseHP*/40,/*LEVEL*/16,/*baseDamage*/45,  /*xpToNextLevel*/179, /*GivenXP*/ 141, Water,/*levelToEvolve*/ 1000, /*Evolved Form*/ NULL);
	Pokemon* Swampert = new Pokemon("Swampert",/*baseHP*/40,/*LEVEL*/16,/*baseDamage*/45,  /*xpToNextLevel*/179, /*GivenXP*/ 141, Water,/*levelToEvolve*/ 36, /*Evolved Form*/ MegaSwampert);
	Pokemon* Marshtomp = new Pokemon("Marshtomp",/*baseHP*/40,/*LEVEL*/16,/*baseDamage*/45, /*xpToNextLevel*/179, /*GivenXP*/ 141, Water,/*levelToEvolve*/ 36, /*Evolved Form*/ Swampert);
	Pokemon* Mudkip = new Pokemon("Mudkip",/*baseHP*/12,/*LEVEL*/1,/*baseDamage*/7,/*xpToNextLevel*/20, /*GivenXP*/ 8, Water,/*levelToEvolve*/ 16, /*Evolved Form*/ Marshtomp);

	Pokemon* MegaBlaziken = new Pokemon("Mega-Blaziken",/*baseHP*/40,/*LEVEL*/16,/*baseDamage*/45, /*xpToNextLevel*/179, /*GivenXP*/ 141, Fire,/*levelToEvolve*/ 1000, /*Evolved Form*/ NULL);
	Pokemon* Blaziken = new Pokemon("Blaziken",/*baseHP*/40,/*LEVEL*/16,/*baseDamage*/45, /*xpToNextLevel*/179, /*GivenXP*/ 141, Fire,/*levelToEvolve*/ 36, /*Evolved Form*/ MegaBlaziken);
	Pokemon* Combusken = new Pokemon("Combusken",/*baseHP*/40,/*LEVEL*/16,/*baseDamage*/45, /*xpToNextLevel*/179, /*GivenXP*/ 141, Fire,/*levelToEvolve*/ 36, /*Evolved Form*/ Blaziken);
	Pokemon* Torchic = new Pokemon("Torchic",/*baseHP*/13,/*LEVEL*/1,/*baseDamage*/6, /*xpToNextLevel*/ 18, /*GivenXP*/ 8, Fire,/*levelToEvolve*/ 16, /*Evolved Form*/ Combusken);

	Pokemon* MegaMetagross = new Pokemon("Mega-Metagross",/*baseHP*/40,/*LEVEL*/16,/*baseDamage*/45,/*xpToNextLevel*/179, /*GivenXP*/ 141, Psychic,/*levelToEvolve*/ 1000, /*Evolved Form*/ NULL);
	Pokemon* Metagross = new Pokemon("Metagross",/*baseHP*/40,/*LEVEL*/16,/*baseDamage*/45,  /*xpToNextLevel*/179, /*GivenXP*/ 141, Psychic,/*levelToEvolve*/ 36, /*Evolved Form*/ MegaMetagross);
	Pokemon* Metang = new Pokemon("Metang",/*baseHP*/40,/*LEVEL*/16,/*baseDamage*/45, /*xpToNextLevel*/179, /*GivenXP*/ 141, Psychic,/*levelToEvolve*/ 36, /*Evolved Form*/ Metagross);
	Pokemon* Beldum = new Pokemon("Beldum",/*baseHP*/14,/*LEVEL*/1,/*baseDamage*/6, /*xpToNextLevel*/20, /*GivenXP*/ 10, Psychic,/*levelToEvolve*/ 16, /*Evolved Form*/ Metang);

	Pokemon* MegaAggron = new Pokemon("Mega-Aagron", /*baseHP*/40,/*LEVEL*/16,/*baseDamage*/45, /*xpToNextLevel*/179, /*GivenXP*/ 141, Steel,/*levelToEvolve*/ 1000, /*Evolved Form*/ NULL);
	Pokemon* Aggron = new Pokemon("Aggron",/*baseHP*/40,/*LEVEL*/16,/*baseDamage*/45,  /*xpToNextLevel*/179, /*GivenXP*/ 234, Steel,/*levelToEvolve*/ 32, /*Evolved Form*/ MegaAggron);
	Pokemon* Lairon = new Pokemon("Lairon", /*baseHP*/40,/*LEVEL*/16,/*baseDamage*/45,  /*xpToNextLevel*/179, /*GivenXP*/ 142, Steel,/*levelToEvolve*/ 32, /*Evolved Form*/ Aggron);
	Pokemon* Aaron = new Pokemon("Aaron",/*baseHP*/12,/*LEVEL*/1,/*baseDamage*/7,/*xpToNextLevel*/19, /*GivenXP*/ 8, Steel,/*levelToEvolve*/ 16, /*Evolved Form*/ Lairon);

	this->PokemonList = { Mudkip, Treecko, Torchic, Zubat, Poochyena, Zigzagoon, Plusle, Shroomish, Tailow, Ralts, Geodude, Oddish, Electrike, Beldum, Aaron, Lairon, Aggron, MegaAggron, Metang, Metagross, MegaMetagross,
	Combusken, Blaziken, MegaBlaziken, Marshtomp, Swampert, MegaSwampert, Grovyle, Sceptile, MegaSceptile, Manectric, Vileplume, Graveler, Golem, Swellow, Kirlia, Gardevoir, Breloom, Linoone, Mightyena, Golbat, Crobat };

	this->PokemonSpawnList = { Zubat, Poochyena, Zigzagoon, Plusle, Shroomish, Tailow, Ralts, Geodude, Oddish, Electrike, Beldum, Aaron, Torchic, Mudkip, Treecko }; //i only includd the base form
}

Pokemon * Pokedex::spawnPokemon()
{
	srand(time(NULL));
	int listRoll;
	listRoll = (rand() % (this->PokemonSpawnList.size())) + 1;

	Pokemon* spawnedPokemon = this->PokemonSpawnList[listRoll];
	return spawnedPokemon;
}