#pragma once
#include "Megastone.h"

MegaStone::MegaStone(string name, string compatiblePokemonName, int count, int price)
{
	this->name = name;
	this->compatiblePokemonName = compatiblePokemonName;
	this->count = count;
	this->price = price;
}
