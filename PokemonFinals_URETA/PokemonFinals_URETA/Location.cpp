#include "Location.h"

Location::Location(std::string name, bool hasPokemonCenter, std::vector<int> topLeftCoordinates, std::vector<int> bottomLeftCoordinates, std::vector<int> topRightCoordinates, std::vector<int> bottomRightCoordinates)
{
	this->name = name;
	this->hasPokemonCenter = hasPokemonCenter;
	this->topLeftCoordinates = topLeftCoordinates;
	this->bottomLeftCoordinates = bottomLeftCoordinates;
	this->topRightCoordinates = topRightCoordinates;
	this->bottomRightCoordinates = bottomRightCoordinates;
}
